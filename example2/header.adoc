:doctype: article
:linkcss:
:stylesheet: {root_}/style.css
:nofooter:
:sectlinks:
:gpg_asc_: {root_}/5FBC7694A76CA1781C39EBE46E1338005450F701.asc

= {title}

[id="basicnav"]
* link:{root_}/index.html[Home]
* link:{root_}/legal.html[Legal Notice]
* link:{outfile_basename_}[This Page]
ifdef::indexlink_[]
* {indexlink_}
endif::[]
